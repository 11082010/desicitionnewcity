﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DataManager {
	public static int MAX_LEVEL = 17;
	public static int MAX_CHARACTER = 10;
	public static int MAX_CHARACTER_PER_LEVEL = 5;
	public static int MAX_PARAM = 4;
	public static int MAX_PARAM_VALUE = 3;
	public static int MAX_PARAM_SKILL = 3;
	public static Color DISABLE_BUTTON_COLOR = new Color (0.35f, 0.35f, 0.35f, 1f);
	public static int MAX_CURRENT_QUEST = 15;

	#region SAVE TO PLAYER PREFS
	public static List<List<int>> unitEntity = new List<List<int>>();
	public static List<int> stars = new List<int>();
	public static List<int> gems = new List<int>();
	public static List<int> skills = new List<int> ();
//	public static List<int> normalLevels = new List<int>();
//	public static List<int> hardLevels = new List<int>();
	public static int unitUnlocked = 2;
	public static List<int> listcurrentUnitIDSpawn = new List<int>();
	public static List<int>listCurrentQuest = new List<int>();
	public static List<int>listClaimedQuest = new List<int>();
	public static int soundAudio = 1;
	public static int musicAudio = 1;
	#endregion

	#region BASE DATA
	public static List<BaseUnitEntity> listBaseUnitEntity = new List<BaseUnitEntity> ();
	public static List<BaseUnitEntity> listCurrentUnitEntity = new List<BaseUnitEntity> ();
	public static List<int> listAllUnitIDSpawn = new List<int>();
	public static List<int> listCoinWhenStart = new List<int>();
	public static bool normalMode = true;
	public static List<int>listQuestGoal = new List<int>();
	#endregion

	public static int currentLevel = 1;

	public static void SaveAllEntity()
	{
		//Stars
		for (int i = 1; i <= MAX_LEVEL; i++) 
			PlayerPrefs.SetInt("stars"+i,stars[i-1]);
		//Gems
		for (int i = 1; i <= MAX_LEVEL; i++)
			PlayerPrefs.SetInt("gems"+i,gems[i-1]);
		//Skill
		for (int i = 1; i < MAX_PARAM_SKILL; i++)
			PlayerPrefs.SetInt ("skills"+i,skills[i-1]);

		// Character parameters
		for (int i = 1; i <= MAX_CHARACTER; i++)
			for (int j = 1; j <= MAX_PARAM; j++)
				PlayerPrefs.SetInt ("unitEntity" + i + j,unitEntity[i-1][j-1]);

		// Character current choose
		for (int i = 1; i <= MAX_CHARACTER_PER_LEVEL; i++) 
		{
			PlayerPrefs.SetInt("listcurrentUnitIDSpawn"+i,listcurrentUnitIDSpawn[i-1]);
		}

		// Current Quest
		for (int i = 1; i <= MAX_CURRENT_QUEST; i++) 
		{
			PlayerPrefs.SetInt("quest"+i,listCurrentQuest[i-1]);
		}

		// Claimed Quest
		for (int i = 1; i <= MAX_CURRENT_QUEST; i++) 
		{
			PlayerPrefs.SetInt("claimedquest"+i,listClaimedQuest[i-1]);
		}
	}

	public static void LoadAllEntity()
	{
		//Stars
		for (int i = 1; i <= MAX_LEVEL; i++) 
			stars.Add(PlayerPrefs.GetInt("stars"+i,0));
		//Gems
		for (int i = 1; i <= MAX_LEVEL; i++) 
			gems.Add(PlayerPrefs.GetInt("gems"+i,0));
		//Skills
		for (int i = 1; i <= MAX_PARAM_SKILL; i++) 
			skills.Add(PlayerPrefs.GetInt("skills"+i,0));

		//Unlock unit
		if (stars [11] > 0)
			unitUnlocked = 10;
		else if (stars [8] > 0)
			unitUnlocked = 8;
		else if (stars [6] > 0)
			unitUnlocked = 7;
		else if (stars [5] > 0)
			unitUnlocked = 5;
		else if (stars [3] > 0)
			unitUnlocked = 4;
		else if (stars [1] > 0)
			unitUnlocked = 3;
		
		unitEntity. Clear ();
		for (int i = 1; i <= MAX_CHARACTER; i++)
		{
			List<int> tempList = new List<int> ();
			for (int j = 1; j <= MAX_PARAM; j++)
				tempList.Add (PlayerPrefs.GetInt ("unitEntity" + i + j, 0));
			unitEntity.Add (tempList);
		}

		listcurrentUnitIDSpawn.Clear ();
		// Character current choose
		for (int i = 1; i <= MAX_CHARACTER_PER_LEVEL; i++) 
		{
			listcurrentUnitIDSpawn.Add(PlayerPrefs.GetInt("listcurrentUnitIDSpawn"+i,0));
		}
		if (unitUnlocked <= MAX_CHARACTER_PER_LEVEL) 
		{
			for (int i = 1; i <= unitUnlocked; i++) 
			{
				listcurrentUnitIDSpawn [i-1] = i;
			}
		}

		listCurrentQuest.Clear ();
		// Get List Current Quest
		for (int i = 1; i <= MAX_CURRENT_QUEST; i++) 
		{
			listCurrentQuest.Add (PlayerPrefs.GetInt("quest"+i,0));
		}

		listClaimedQuest.Clear ();
		// Get List Claimed Quest
		for (int i = 1; i <= MAX_CURRENT_QUEST; i++) 
		{
			listClaimedQuest.Add (PlayerPrefs.GetInt("claimedquest"+i,0));
		}

		// SET DEFAULT DATA
		SetCurrentUnitEntity ();
		SetBaseQuest ();
		listCoinWhenStart = new List<int> (){60,120,120,80,130,90,115,200,280,200,100,200,100,250,115};
		waveStringListAllLevel.Add (waveStringListLevel1);
		waveStringListAllLevel.Add (waveStringListLevel2);
		waveStringListAllLevel.Add (waveStringListLevel3);
		waveStringListAllLevel.Add (waveStringListLevel4);
		waveStringListAllLevel.Add (waveStringListLevel5);
		waveStringListAllLevel.Add (waveStringListLevel6);
		waveStringListAllLevel.Add (waveStringListLevel7);
		waveStringListAllLevel.Add (waveStringListLevel8);
		waveStringListAllLevel.Add (waveStringListLevel9);
		waveStringListAllLevel.Add (waveStringListLevel10);
		waveStringListAllLevel.Add (waveStringListLevel11);
		waveStringListAllLevel.Add (waveStringListLevel12);
		waveStringListAllLevel.Add (waveStringListLevel13);
		waveStringListAllLevel.Add (waveStringListLevel14);
		waveStringListAllLevel.Add (waveStringListLevel15);
	}

	public static void SaveAudioOption()
	{
		PlayerPrefs.SetInt ("music",DataManager.musicAudio);
		PlayerPrefs.SetInt ("sound",DataManager.soundAudio);
	}

	public static void LoadAudioOption()
	{
		DataManager.musicAudio = PlayerPrefs.GetInt("music",1);
		DataManager.soundAudio = PlayerPrefs.GetInt ("sound",1);
	}

	public static void ResetUnitEntity()
	{
		for (int i = 1; i <= MAX_CHARACTER; i++)
			for (int j = 1; j <= MAX_PARAM; j++)
				unitEntity[i-1][j-1] = 0;
	}

	public static void ResetSkillEntity()
	{
		for (int i = 1; i <= MAX_PARAM_SKILL; i++)
			skills [i - 1] = 0;
	}


	public static int StarUsedAmount()
	{
		int _star = 0;
		for (int i = 1; i <= MAX_CHARACTER; i++)
			for (int j = 1; j <= MAX_PARAM; j++)
				_star += unitEntity[i-1][j-1];
		return _star;
	}

	public static int StarFreeAmount ()
	{
		return (StarHaveAmount () - StarUsedAmount ());
	}

	public static int StarHaveAmount ()
	{
		int _star = 0;
		for (int i = 1; i <= MAX_LEVEL; i++)
			_star += stars [i - 1];
		return _star;
	}

	public static int GemHaveAmount ()
	{
		int _gem = 0;
		for (int i = 1; i <= MAX_LEVEL; i++)
			_gem += gems [i - 1];
		return _gem;
	}

	public static int GemUsedAmount()
	{
		int _gem = 0;
		for (int i = 1; i <= MAX_PARAM_SKILL; i++) 
		{
			if (skills [i-1] == 1)
				_gem += 1;
			else if (skills [i-1] == 2)
				_gem += 3;
			else if (skills [i-1] == 3)
				_gem += 6;
		}
		return _gem;
	}

	public static int GemFreeAmount ()
	{
		return (GemHaveAmount () - GemUsedAmount ());
	}

	public static void SetBaseQuest()
	{
		listQuestGoal = new List<int> (15) {
			20, // Complete 20 stars
			15, // Complete 15 levels
			10, // Complete 10 levels
			5,  // Complete 5 levels
			1,  // Defeat second boss

			1,  // Defeat first boss
			1,  // Defeat third boss
			100,// Freeze 100 zombies
			40, // Place 40 mines
			10, // Kill 10 zombies with rocket

			5, // Kill 50 zombies
			10,// Kill 100 zombies
			15,// Kill 150 zombies
			2,	// Buy all upgrades for 2 unit
			3,  // Buy all skill upgrades
		};
	}
	public static void IncreaseQuest (int index)
	{
		if (index == 1)
			listCurrentQuest [0]++;
		else if (index == 2)
			listCurrentQuest [1]++;
		else if (index == 3)
			listCurrentQuest [2]++;
		else if (index == 4)
			listCurrentQuest [3]++;
		else if (index == 5)
			listCurrentQuest [4]++;
		else if (index == 6)
			listCurrentQuest [5]++;
		else if (index == 7)
			listCurrentQuest [6]++;
		else if (index == 8)
			listCurrentQuest [7]++;
		else if (index == 9)
			listCurrentQuest [8]++;
		else if (index == 10)
			listCurrentQuest [9]++;
		else if (index == 11)
			listCurrentQuest [10]++;
		else if (index == 12)
			listCurrentQuest [11]++;
		else if (index == 13)
			listCurrentQuest [12]++;
		else if (index == 14)
			listCurrentQuest [13]++;
		else if (index == 15)
			listCurrentQuest [14]++;

		SaveAllEntity ();
	}

	public static void SetBaseUnitEntity()
	{

//		Health		100	150	200	100	150	250	300	200	150	200
//		Dame		20	30	20	40	13	15	30	50	50	70
//		AtkSpeed	30	25	20	30	70	70	25	30	30	50
//		Range		350	250	1	650	350	50	200	450	500	575
//		TrueRange	875	625	200	1625875	200	500	1000	1250	1440
		// Set unit base
		//(int _baseHealth,int _baseDame,float _baseRange,float _baseAtkSpeed,int _baseCriticalChance,int _baseHealthRegen,int _baseExplode,int _baseCost,int _freezeChance)
		BaseUnitEntity unit1 = new BaseUnitEntity(100,20,875f,0.3f,0,0,0,50,1,0);
		BaseUnitEntity unit2 = new BaseUnitEntity(150,30,650f,0.25f,0,0,0,65,1,0);
		BaseUnitEntity unit3 = new BaseUnitEntity(200,20,200f,0.2f,0,0,0,70,1,0);
		BaseUnitEntity unit4 = new BaseUnitEntity(100,40,1625f,0.3f,0,0,0,100,1,0);
		BaseUnitEntity unit5 = new BaseUnitEntity(150,13,875f,0.7f,0,0,0,90,1,0);
		BaseUnitEntity unit6 = new BaseUnitEntity(300,30,200f,1.54f,0,0,0,100,1,0); 
		BaseUnitEntity unit7 = new BaseUnitEntity(250,15,500f,0.7f,0,0,0,90,1,0);
		BaseUnitEntity unit8 = new BaseUnitEntity(200,50,1000f,0.3f,0,0,0,150,1,0); 
		BaseUnitEntity unit9 = new BaseUnitEntity(150,50,1250f,0.3f,0,0,0,100,1,0); 
		BaseUnitEntity unit10 = new BaseUnitEntity(200,70,1440f,0.5f,0,0,0,120,1,0); 

		listBaseUnitEntity.Clear ();
		listBaseUnitEntity.Add (unit1);
		listBaseUnitEntity.Add (unit2);
		listBaseUnitEntity.Add (unit3);
		listBaseUnitEntity.Add (unit4);
		listBaseUnitEntity.Add (unit5);
		listBaseUnitEntity.Add (unit6);
		listBaseUnitEntity.Add (unit7);
		listBaseUnitEntity.Add (unit8);
		listBaseUnitEntity.Add (unit9);
		listBaseUnitEntity.Add (unit10);
	}

	static float INCREASE_AMOUNT = 0.1f;
	static int DECREASE_COST_AMOUNT = 5;
	static int HEALTH_REGEN_AMOUNT = 3;

	public static void SetCurrentUnitEntity()
	{
		SetBaseUnitEntity ();
		listCurrentUnitEntity.Clear ();
		listCurrentUnitEntity.AddRange (listBaseUnitEntity);
		for (int i=0; i<unitEntity.Count; i++) {
			switch (i) {
			case 0: // UNIT 1
				listCurrentUnitEntity[i].baseCriticalChance = (int)unitEntity[i][0];
				listCurrentUnitEntity[i].baseDame += (int)(listCurrentUnitEntity[i].baseDame * INCREASE_AMOUNT * unitEntity[i][1]);
				listCurrentUnitEntity[i].basePrice -= (int)(DECREASE_COST_AMOUNT * unitEntity[i][2]);
				listCurrentUnitEntity[i].baseRange += (int)(listCurrentUnitEntity[i].baseRange * INCREASE_AMOUNT * unitEntity[i][3]);
				break;
			case 1: // UNIT 2
				listCurrentUnitEntity[i].baseRange += (int)(listCurrentUnitEntity[i].baseRange * INCREASE_AMOUNT * unitEntity[i][0]);
				// 2 is additional bullet
				listCurrentUnitEntity[i].baseBullet += (int)(listCurrentUnitEntity[i].baseBullet * unitEntity[i][1]);
				listCurrentUnitEntity[i].baseHealth += (int)(listCurrentUnitEntity[i].baseHealth * INCREASE_AMOUNT * unitEntity[i][2]);
				listCurrentUnitEntity[i].baseDame += (int)(listCurrentUnitEntity[i].baseDame * INCREASE_AMOUNT * unitEntity[i][3]);
				break;
			case 2: // UNIT 3
				listCurrentUnitEntity[i].baseHealth += (int)(listCurrentUnitEntity[i].baseHealth * INCREASE_AMOUNT * unitEntity[i][0]);
				listCurrentUnitEntity[i].baseHealthRegenPerSecond += (int)(HEALTH_REGEN_AMOUNT * unitEntity[i][1]);
				// 3 is reduce cost
				listCurrentUnitEntity[i].basePrice -= (int)(DECREASE_COST_AMOUNT * unitEntity[i][2]);
				listCurrentUnitEntity[i].baseExplode += (int)(unitEntity[i][3]);
				break; 

			case 3: // UNIT 4
				listCurrentUnitEntity[i].baseCriticalChance += (int)(unitEntity[i][0]);
				listCurrentUnitEntity[i].baseAtkSpeed += (float)(listCurrentUnitEntity[i].baseAtkSpeed * INCREASE_AMOUNT * unitEntity[i][1]);
				listCurrentUnitEntity[i].baseDame += (int)(listCurrentUnitEntity[i].baseDame * INCREASE_AMOUNT * unitEntity[i][2]);
				listCurrentUnitEntity[i].baseFreezeChance += (int)(unitEntity[i][3]);
				break; 

			case 4: // UNIT 5
				listCurrentUnitEntity[i].baseRange += (int)(listCurrentUnitEntity[i].baseRange * INCREASE_AMOUNT * unitEntity[i][0]);
				listCurrentUnitEntity[i].baseDame += (int)(listCurrentUnitEntity[i].baseDame * INCREASE_AMOUNT * unitEntity[i][1]);
				listCurrentUnitEntity[i].baseHealth += (int)(listCurrentUnitEntity[i].baseHealth * INCREASE_AMOUNT * unitEntity[i][2]);
				listCurrentUnitEntity[i].baseCriticalChance += (int)(unitEntity[i][3]);
				break; 

			case 5: // UNIT 6
				listCurrentUnitEntity[i].baseHealth += (int)(listCurrentUnitEntity[i].baseHealth * INCREASE_AMOUNT * unitEntity[i][0]);
				listCurrentUnitEntity[i].baseDame += (int)(listCurrentUnitEntity[i].baseDame * INCREASE_AMOUNT * unitEntity[i][1]);
				listCurrentUnitEntity[i].baseHealthRegenPerSecond += (int)(HEALTH_REGEN_AMOUNT * unitEntity[i][2]);
				listCurrentUnitEntity[i].baseExplode += (int)unitEntity[i][3];
				break; 

			case 6: // UNIT 7
				listCurrentUnitEntity[i].baseAtkSpeed += (float)(listCurrentUnitEntity[i].baseAtkSpeed * INCREASE_AMOUNT * unitEntity[i][0]);
				listCurrentUnitEntity[i].baseHealth += (int)(listCurrentUnitEntity[i].baseHealth * INCREASE_AMOUNT * unitEntity[i][1]);
				listCurrentUnitEntity[i].baseHealthRegenPerSecond += (int)(HEALTH_REGEN_AMOUNT * unitEntity[i][2]);
				listCurrentUnitEntity[i].baseDame += (int)(listCurrentUnitEntity[i].baseDame * INCREASE_AMOUNT *unitEntity[i][3]);
				break; 

			case 7: // UNIT 8
				listCurrentUnitEntity[i].baseDame += (int)(listCurrentUnitEntity[i].baseDame * INCREASE_AMOUNT * unitEntity[i][0]);
				listCurrentUnitEntity[i].baseRange += (int)(listCurrentUnitEntity[i].baseRange * INCREASE_AMOUNT * unitEntity[i][1]);
				listCurrentUnitEntity[i].baseBullet += (int)(listCurrentUnitEntity[i].baseBullet * unitEntity[i][2]);
				listCurrentUnitEntity[i].baseHealth += (int)(listCurrentUnitEntity[i].baseHealth * INCREASE_AMOUNT *unitEntity[i][3]);
				break; 

			case 8: // UNIT 9
				listCurrentUnitEntity[i].baseDame += (int)(listCurrentUnitEntity[i].baseDame * INCREASE_AMOUNT * unitEntity[i][0]);
				listCurrentUnitEntity[i].baseHealth += (int)(listCurrentUnitEntity[i].baseHealth * INCREASE_AMOUNT * unitEntity[i][1]);
				listCurrentUnitEntity[i].baseRange += (int)(listCurrentUnitEntity[i].baseRange * INCREASE_AMOUNT * unitEntity[i][2]);
				listCurrentUnitEntity[i].basePrice -= (int)(DECREASE_COST_AMOUNT *unitEntity[i][3]);
				break;

			case 9: // UNIT 10
				listCurrentUnitEntity[i].baseFreezeChance += (int)(unitEntity[i][0]);
				listCurrentUnitEntity[i].baseAtkSpeed += (float)(listCurrentUnitEntity[i].baseAtkSpeed * INCREASE_AMOUNT * unitEntity[i][1]);
				listCurrentUnitEntity[i].baseDame += (int)(listCurrentUnitEntity[i].baseDame * INCREASE_AMOUNT * unitEntity[i][2]);
				listCurrentUnitEntity[i].baseRange += (int)(listCurrentUnitEntity[i].baseRange * INCREASE_AMOUNT *unitEntity[i][3]);
				break; 
			}
		}
	}
	#region ZOMBIE WAVE
	public static List<string> waveStringListLevel1 = new List<string> () {
			"2,1,1,500",
            "2,1,1,600",
            "2,1,1,700",
            "2,1,1,100",
            "2,1,1,100",
            "2,1,1,900",
            "2,2,1,500",
            "2,1,1,150",
            "2,1,1,150",
            "2,2,1,0",
	};

	public static List<string> waveStringListLevel2 = new List<string> () {
            "2,1,1,250",
            "2,1,1,250",
            "2,2,1,600",
            "2,1,1,100",
            "2,1,1,350",			
            "2,1,1,150",
            "2,1,1,150",
            "2,1,1,500",
            "2,2,1,150",
            "2,2,1,500",			
            "2,1,1,150",
            "2,1,1,400",
            "2,2,1,150",
            "2,1,1,150",
            "2,1,1,600",			
            "2,3,1,0",
	};
	public static List<string> waveStringListLevel3 = new List<string> () {
			"1,1,1,100",
            "3,1,1,150",
			"1,1,1,400",
            "3,1,1,600",
            "2,2,1,700",			
            "2,1,1,60",
			"1,1,1,400",
            "3,1,1,400",
			"1,2,1,400",
            "3,2,1,800",			
            "3,3,1,500",
            "1,2,1,150",
            "1,2,1,900",
            "2,5,1,900",
            "3,1,1,150",			
            "3,1,1,150",
            "3,1,1,150",
            "1,1,1,50",
            "2,1,1,100",
            "1,1,1,50",			
            "2,1,1,350",
            "2,3,1,0",
	};
	public static List<string> waveStringListLevel4 = new List<string> () {
			"1,1,1,150",
            "1,1,1,150",
            "1,1,1,100",
            "1,1,1,750",			
            "2,4,1,350",
            "1,2,1,200",
            "1,1,1,120",
            "1,1,1,120",
            "1,5,1,800",
            "2,1,1,150",
            "2,1,1,150",			
            "2,1,1,250",
            "2,3,1,500",
            "3,1,1,150",
            "3,1,1,150",
            "3,1,1,350",			
            "3,2,1,50",
            "1,4,1,150",
            "1,1,1,100",
            "1,1,1,100",
            "1,3,1,700",			
            "3,2,1,150",
            "3,1,1,120",
            "3,1,1,120",
            "2,3,1,50",
            "3,5,1,900",			
            "1,4,1,50",
            "3,4,1,50",
            "2,4,1,300",
			"1,1,1,300",
			"2,1,1,300",			
			"3,1,1,300",
			"2,1,1,300",
			"3,1,1,300",
			"1,1,1,300",
			"3,2,1,300",			
			"2,2,1,300",
			"1,2,1,300",
			"3,1,1,300",
			"2,1,1,300",
            "1,1,1,250",			
            "1,3,1,80",
            "3,3,1,80",
            "2,3,1,80",
	};
	public static List<string> waveStringListLevel5 = new List<string> () {
			"2,1,1,30",
            "3,1,1,120",
            "3,1,1,130",
            "2,1,1,120",
            "2,2,1,150",
            "3,2,1,500",
            "1,5,1,400",
            "1,1,1,140",
            "1,1,1,140",
            "1,2,1,800",
            "2,3,1,70",
            "3,3,1,500",
            "3,1,1,250",
            "2,1,1,250",
            "1,1,1,250",
            "2,1,1,350",
            "2,4,1,230",
            "3,4,1,230",
            "1,4,1,800",
            "2,2,1,50",
            "3,2,1,50",
            "1,2,1,150",
            "3,1,1,350",
            "2,1,1,350",
            "1,1,1,150",
            "2,3,1,150",
            "1,2,1,150",
            "3,2,1,800",
            "2,1,1,120",
            "2,2,1,120",
            "2,1,1,100",
            "2,1,1,200",
            "2,4,1,120",
            "2,5,1,600",
            "1,1,1,100",
            "1,1,1,100",
            "1,2,1,100",
            "1,1,1,100",
            "1,2,1,150",
            "1,3,1,150",
            "1,5,1,800",
            "3,1,1,100",
            "3,3,1,200",
            "3,5,1,150",
            "3,1,1,100",
            "3,1,1,100",
            "3,2,1,900",
			"2,3,1,0",
	};
	
	public static List<string> waveStringListLevel6 = new List<string> () {
		 	"2,6,1,400",
            "2,1,1,150",
            "2,1,1,150",
            "2,1,1,500",
            "3,2,1,100",
            "3,1,1,100",
            "3,6,1,150",
            "3,6,1,800",
            "2,3,1,400",
            "2,6,1,400",
            "2,6,1,400",
            "2,1,1,350",
            "2,1,1,1000",
            "1,1,1,200",
            "1,1,1,200",
            "1,2,1,400",
            "1,6,1,800",
            "3,6,1,70",
            "1,6,1,70",
            "2,6,1,200",
            "3,2,1,70",
            "1,2,1,70",
            "2,2,1,70",
            "1,1,1,70",
            "3,1,1,70",
            "2,1,1,300",
            "2,5,1,70",
            "3,6,1,70",
            "1,5,1,70",
            "2,6,1,70",
            "3,5,1,70",
            "1,6,1,700",
            "2,9,1,500",
            "1,3,1,70",
            "3,3,1,70",
            "2,3,1,500",
            "1,9,1,60",
            "3,9,1,60",
            "2,9,1,0",
	};
	public static List<string> waveStringListLevel7 = new List<string> () {
		 	"2,2,1,250",
            "2,2,1,250",
            "2,2,1,350",
            "3,6,1,300",
            "3,4,1,80",
            "2,4,1,300",
            "2,6,1,150",
            "2,2,1,150",
            "2,5,1,400",
            "1,4,1,200",
            "2,3,1,100",
            "2,5,1,400",
            "3,6,1,100",
            "3,6,1,100",
            "3,5,1,500",
            "2,2,1,70",
            "3,2,1,70",
            "1,2,1,70",
            "2,2,1,70",
            "3,2,1,350",
            "3,3,1,80",
            "1,3,1,80",
            "2,3,1,700",
            "3,9,1,80",
            "1,9,1,80",
            "2,9,1,600",
            "2,6,1,100",
            "1,5,1,100",
            "3,6,1,100",
            "2,5,1,100",
            "1,6,1,100",
            "3,5,1,900",
            "2,3,1,80",
            "1,9,1,80",
            "3,3,1,80",
            "2,9,1,80",
            "1,3,1,80",
            "3,9,1,400",
            "1,6,1,80",
            "3,6,1,80",
            "2,6,1,80",
            "3,6,1,80",
            "1,6,1,80",
            "2,6,1,700",
            "3,3,1,80",
            "1,5,1,80",
            "2,3,1,80",
            "3,5,1,80",
            "1,3,1,80",
            "2,5,1,80",
            "2,3,1,80",
            "1,5,1,80",
            "3,3,1,80",
            "2,5,1,80",
            "1,3,1,80",
            "3,5,1,0",
	};
	public static List<string> waveStringListLevel8 = new List<string> () {
			"1,2,1,30",
            "3,2,1,350",
            "1,6,1,120",
            "1,5,1,50",
            "3,3,1,600",
            "3,6,1,80",
            "3,5,1,600",
            "2,7,1,1000",
            "1,9,1,100",
            "3,9,1,100",
            "2,9,1,700",
            "3,2,1,70",
            "1,2,1,70",
            "2,2,1,70",
            "3,2,1,70",
            "2,2,1,70",
            "1,2,1,450",
            "3,7,1,60",
            "2,7,1,60",
            "1,7,1,1000",
            "1,3,1,60",
            "3,3,1,100",
            "1,5,1,60",
            "3,5,1,100",
            "1,3,1,60",
            "3,3,1,100",
            "1,5,1,60",
            "3,5,1,700",
            "1,9,1,0",
            "2,9,1,0",
            "3,9,1,100",
            "1,7,1,0",
            "2,7,1,0",
            "3,7,1,1000",
            "1,6,1,80",
            "3,6,1,80",
            "2,6,1,100",
            "3,5,1,80",
            "1,5,1,80",
            "2,5,1,400",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,70",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,0",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,600",
            "1,9,1,50",
            "3,9,1,50",
            "2,9,1,50",
            "3,9,1,50",
            "1,9,1,600",
            "2,7,1,120",
            "2,7,1,120",
            "2,7,1,0",
	};
	public static List<string> waveStringListLevel9 = new List<string> () {
			"2,7,1,500",
            "3,2,1,20",
            "1,2,1,50",
            "3,2,1,20",
            "1,2,1,350",
            "2,4,1,50",
            "2,6,1,70",
            "2,6,1,70",
            "2,5,1,600",
            "3,9,1,250",
            "1,9,1,250",
            "2,7,1,900",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,90",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,90",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,120",
            "1,5,1,0",
            "2,5,1,0",
            "3,5,1,90",
            "1,5,1,0",
            "2,5,1,0",
            "3,5,1,90",
            "1,5,1,0",
            "2,5,1,0",
            "3,5,1,700",
            "1,9,1,70",
            "1,6,1,70",
            "1,7,1,70",
            "1,6,1,150",
            "3,9,1,70",
            "3,6,1,70",
            "3,7,1,70",
            "3,6,1,500",
            "2,4,1,70",
            "2,4,1,70",
            "2,4,1,70",
            "1,4,1,70",
            "3,4,1,70",
            "1,4,1,70",
            "3,4,1,700",
            "1,9,1,0",
            "2,9,1,0",
            "3,9,1,100",
            "1,5,1,0",
            "2,5,1,0",
            "3,5,1,100",
            "1,9,1,0",
            "2,9,1,0",
            "3,9,1,100",
            "1,7,1,0",
            "2,7,1,0",
            "3,7,1,0",
	};
	public static List<string> waveStringListLevel10 = new List<string> () {
			"1,3,1,0",
            "3,3,1,500",
            "1,5,1,0",
            "3,5,1,300",
            "1,6,1,70",
            "1,6,1,150",
            "3,6,1,70",
            "3,6,1,500",
            "2,9,1,350",
            "2,6,1,70",
            "2,6,1,300",
            "1,9,1,0",
            "2,9,1,0",
            "3,9,1,900",
            "1,3,1,60",
            "3,3,1,60",
            "2,3,1,60",
            "3,3,1,60",
            "1,3,1,60",
            "2,5,1,60",
            "3,5,1,60",
            "1,5,1,900",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,100",
            "1,9,1,0",
            "2,9,1,0",
            "3,9,1,300",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,100",
            "1,9,1,0",
            "2,9,1,0",
            "3,9,1,800",
            "1,3,1,30",
            "3,3,1,30",
            "2,3,1,60",
            "1,6,1,30",
            "2,6,1,30",
            "3,6,1,60",
            "1,9,1,30",
            "3,9,1,60",
            "1,3,1,30",
            "3,3,1,30",
            "2,3,1,60",
            "1,6,1,30",
            "2,6,1,30",
            "3,6,1,60",
            "1,9,1,30",
            "3,9,1,1000",
			"2,9,1,0",
	};
	public static List<string> waveStringListLevel11 = new List<string> () {
			"2,8,1,900",
            "2,3,1,250",
            "2,2,1,150",
            "2,2,1,150",
            "2,3,1,70",
            "3,2,1,250",
            "3,2,1,250",
            "3,2,1,50",
            "2,9,1,700",
            "1,3,1,150",
            "1,2,1,150",
            "1,2,1,150",
            "1,3,1,100",
            "2,8,1,500",
            "3,3,1,150",
            "3,6,1,100",
            "3,6,1,100",
            "3,8,1,700",
            "2,7,1,400",
            "3,7,1,1000",
            "1,6,1,70",
            "1,6,1,70",
            "1,6,1,100",
            "1,8,1,500",
            "2,3,1,90",
            "2,6,1,90",
            "2,3,1,90",
            "2,3,1,500",
            "1,6,1,90",
            "1,6,1,90",
            "1,9,1,90",
            "1,6,1,90",
            "3,6,1,90",
            "3,6,1,90",
            "3,9,1,90",
            "2,6,1,90",
            "2,6,1,90",
            "2,9,1,90",
            "1,8,1,0",
            "2,8,1,0",
            "3,8,1,100",
            "1,7,1,0",
            "2,7,1,0",
            "3,7,1,0",
	};
	public static List<string> waveStringListLevel12 = new List<string> () {
			"1,9,1,0",
            "3,9,1,500",
            "1,4,1,0",
            "3,4,1,100",
            "1,6,1,0",
            "3,6,1,500",
            "2,8,1,500",
            "2,9,1,100",
            "2,9,1,800",
            "2,7,1,500",
            "1,8,1,40",
            "3,8,1,100",
            "3,6,1,40",
            "1,6,1,100",
            "1,6,1,40",
            "3,6,1,800",
            "2,8,1,100",
            "2,8,1,100",
            "2,6,1,100",
            "2,6,1,100",
            "2,6,1,100",
            "2,6,1,100",
            "1,8,1,300",
            "1,9,1,150",
            "1,9,1,360",
            "1,7,1,900",
            "3,3,1,40",
            "1,3,1,40",
            "2,3,1,40",
            "3,6,1,40",
            "1,6,1,40",
            "2,6,1,40",
            "3,4,1,40",
            "1,4,1,40",
            "2,4,1,40",
            "1,8,1,40",
            "3,8,1,40",
            "2,8,1,700",
            "3,9,1,80",
            "1,9,1,80",
            "2,9,1,80",
            "1,7,1,80",
            "3,7,1,80",
            "2,7,1,80",
            "1,8,1,80",
            "2,8,1,80",
            "3,8,1,80",
            "2,9,1,80",
            "1,9,1,80",
            "3,9,1,80",
            "2,9,1,80",
            "1,9,1,0",
	};
	public static List<string> waveStringListLevel13= new List<string> () {
			"1,9,1,500",
            "1,6,1,200",
            "1,6,1,200",
            "1,8,1,900",
            "3,6,1,150",
            "3,6,1,250",
            "3,9,1,200",
            "3,9,1,200",
            "3,6,1,100",
            "3,6,1,900",
            "1,7,1,500",
            "3,6,1,80",
            "3,6,1,80",
            "3,8,1,350",
            "2,9,1,250",
            "2,6,1,100",
            "2,6,1,200",
            "2,8,1,600",
            "3,9,1,100",
            "3,9,1,600",
            "3,10,1,1000",
            "1,9,1,250",
            "1,6,1,100",
            "1,6,1,150",
            "1,8,1,500",
            "1,10,1,800",
            "2,9,1,100",
            "2,7,1,100",
            "2,9,1,100",
            "2,6,1,100",
            "2,6,1,100",
            "2,8,1,100",
            "2,6,1,100",
            "2,6,1,600",
            "1,10,1,0",
            "2,10,1,0",
            "3,10,1,1000",
            "1,4,1,0",
            "2,4,1,0",
            "3,4,1,80",
            "1,4,1,0",
            "2,4,1,0",
            "3,4,1,150",
            "1,9,1,0",
            "2,9,1,0",
            "3,9,1,80",
            "1,9,1,0",
            "2,9,1,0",
            "3,9,1,80",
            "1,8,1,0",
            "2,8,1,0",
            "3,8,1,80",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,80",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,400",
            "1,10,1,0",
            "2,10,1,0",
            "3,10,1,120",
            "1,10,1,0",
            "2,10,1,0",
            "3,10,1,0",
	};
	public static List<string> waveStringListLevel14= new List<string> () {
			"3,10,1,1000",
            "3,6,1,100",
            "3,6,1,100",
            "3,8,1,700",
            "2,9,1,500",
            "2,6,1,150",
            "2,6,1,150",
            "3,9,1,70",
            "2,9,1,150",
            "2,10,1,100",
            "3,9,1,100",
            "3,7,1,100",
            "3,9,1,100",
            "3,7,1,800",
            "1,10,1,500",
            "1,9,1,100",
            "1,6,1,100",
            "1,9,1,100",
            "1,6,1,100",
            "1,9,1,100",
            "1,6,1,800",
            "2,10,1,300",
            "2,9,1,100",
            "2,6,1,100",
            "2,9,1,100",
            "2,6,1,100",
            "2,9,1,100",
            "2,6,1,300",
            "3,10,1,300",
            "3,9,1,100",
            "3,6,1,100",
            "3,9,1,100",
            "3,6,1,100",
            "3,9,1,100",
            "3,6,1,500",
            "1,10,1,0",
            "2,10,1,0",
            "3,10,1,200",
            "1,9,1,0",
            "2,9,1,0",
            "3,9,1,100",
            "1,7,1,0",
            "2,7,1,0",
            "3,7,1,100",
            "1,9,1,0",
            "2,9,1,0",
            "3,9,1,100",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,100",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,150",
            "1,10,1,0",
            "2,10,1,0",
            "3,10,1,0",
	};
	public static List<string> waveStringListLevel15= new List<string> () {
			"2,8,1,350",
            "2,6,1,250",
            "2,6,1,250",
            "2,9,1,250",
            "2,7,1,1000",
            "2,6,1,100",
            "2,6,1,100",
            "2,6,1,500",
            "1,9,1,300",
            "1,8,1,250",
            "1,6,1,100",
            "1,6,1,350",
            "1,7,1,1000",
            "1,6,1,100",
            "1,6,1,100",
            "1,6,1,150",
            "1,9,1,150",
            "1,8,1,500",
            "3,8,1,150",
            "3,6,1,150",
            "3,6,1,150",
            "3,6,1,150",
            "3,9,1,500",
            "1,6,1,0",
            "2,6,1,0",
            "3,6,1,100",
            "1,8,1,0",
            "2,8,1,0",
            "3,8,1,100",
            "1,9,1,0",
            "2,9,1,0",
            "3,9,1,600",
            "1,7,1,0",
            "2,7,1,0",
            "3,7,1,600",
            "1,10,1,0",
            "2,10,1,0",
            "3,10,1,800",
            "1,9,1,80",
            "1,8,1,80",
            "1,9,1,80",
            "1,6,1,80",
            "1,6,1,80",
            "1,6,1,80",
            "1,9,1,300",
            "1,10,1,0",
            "2,10,1,0",
            "3,10,1,1000",
			"2,10,1,100",
			"2,10,1,0",
	};

	public static List<List<string>> waveStringListAllLevel = new List<List<string>> ();
}
#endregion

public class BaseUnitEntity
{
	public int baseHealth;
	public int baseDame;
	public float baseRange;
	public float baseAtkSpeed;

	public int baseCriticalChance;
	public int baseHealthRegenPerSecond;
	public int baseExplode;
	public int basePrice;
	public int baseBullet;
	public int baseFreezeChance;

	public BaseUnitEntity(int _baseHealth,int _baseDame,float _baseRange,float _baseAtkSpeed,int _baseCriticalChance,int _baseHealthRegen,int _baseExplode,int _baseCost,int _baseBullet,int _baseFreezeChance)
	{
		baseHealth = _baseHealth;
		baseDame = _baseDame;
		baseRange = _baseRange;
		baseAtkSpeed = _baseAtkSpeed;
		baseCriticalChance = _baseCriticalChance;
		baseHealthRegenPerSecond = _baseHealthRegen;
		baseExplode = _baseExplode;
		basePrice = _baseCost;
		baseBullet = _baseBullet;
		baseFreezeChance = _baseFreezeChance;
	}
}

public enum SPAWN_POSITION 
{
	TOP = 1,
	MID = 2,
	BOT = 3,
}
public class SpawnEntity
{
	public SPAWN_POSITION line;
	public int zombieIndex;
	public float timeSpawnAfterLast; // For 1/100 Second
	public SpawnEntity(int _line,int _zombieIndex, float _time)
	{
		line = (SPAWN_POSITION)_line;
		zombieIndex = _zombieIndex;
		timeSpawnAfterLast = _time;
	}
}